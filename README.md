<head>
  <link rel="stylesheet" href="assets/styles.css">
</head>
<div class="title">
    <img src="assets/uni-logo.png" alt="image_description" style="width: 60%;">
    <h1>Projektet</h1>
</div>

- [Recreating a country's flag and it's national anthem in MATLAB/GNU Octave](./Flag/)
- [Mandelbrot Set implementation in MATLAB/GNU Octave](./Mandelbrot/)
- [Fast Fourier Transform implementation in MATLAB/GNU Octave](./FFT/)
- [Lorenz System implementation in MATLAB/GNU Octave](./Lorenz/)
